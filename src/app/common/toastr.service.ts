import { Injectable } from '@angular/core';
declare let toastr: any;

@Injectable({
  providedIn: 'root'
})
export class ToastrService {

  constructor() { }
success(message: string, title?: string) {
  toastr.success(); }

info(message: string, title?: string) {
  toastr.info(); }

warning(message: string, title?: string) {
  toastr.warning(); }

error(message: string, title?: string) {
  toastr.error(); }

}
