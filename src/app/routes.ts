import { Routes} from '@angular/router';
import {
  FilmListComponent,
  FilmDetailsComponent,
  CreateFilmComponent,
  FilmRouteActivator,
  FilmListResolverService
} from './films/index';

import { Error404Component } from './error/error404.component';



export const appRoutes: Routes = [
  {path: 'films/new', component: CreateFilmComponent,
canDeactivate: ['canDeactivateCreateFilm']},
{ path: 'films', component: FilmListComponent, resolve: {films: FilmListResolverService} },
{ path: 'films/:id', component: FilmDetailsComponent, canActivate: [FilmRouteActivator]},
{path: '404', component: Error404Component},
{ path: '', redirectTo: '/films', pathMatch: 'full'},
{path: 'user', loadChildren: './user/user.module#UserModule'}
];
