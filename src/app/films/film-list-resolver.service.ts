import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { FilmService } from './shared/film.service';
import { map } from 'rxjs/operators';
import { escapeRegExp } from '@angular/compiler/src/util';
@Injectable({
  providedIn: 'root'
})
export class FilmListResolverService {
  constructor(private filmService: FilmService) { }
 resolve(){
return this.filmService.getFilms().pipe(map(films =>films));

 }

}
// Through the resolver we are waiting for all the data to come from the server
// before we load it on th page. Otherwise without it , it might load partially.
