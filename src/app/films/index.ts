
export * from './create-film.component';
export * from './film-thumbnail.component';
export * from './film-list-resolver.service';
export * from './film-list.component';
export * from './shared/index';
export * from './film-details/index';
