import { Component, Input, Output, EventEmitter } from "@angular/core";
import { IFilm} from './shared/index';

@Component({
  selector: "app-film-thumbnail",
  templateUrl: "./film-thumbnail.component.html",
  styles: [
    `
      .pad-left {
        margin-left: 10px;
      }
      .card {
        width: 420px;
        height: 500px;
      }
      .card-img-top {
        height: 262px;
        width: 400px;
      }
    `
  ]
})
export class FilmThumbnailComponent {
  @Input() film: IFilm;

  getStartTimeStyle(): any {
    if (this.film && this.film.time === "4:00 pm") {
      return { color: "#F6DF12", "font -weight": "bold" };
    } else {
      return {};
    }
  }
  // someProperty: any = 'some value';

  // logFoo(){
  //   console.log('foo');
  // }
}

// This is a public method which can be seen on the parent component!
