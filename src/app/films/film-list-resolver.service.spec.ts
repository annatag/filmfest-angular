import { TestBed } from '@angular/core/testing';

import { FilmListResolverService } from './film-list-resolver.service';

describe('FilmListResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilmListResolverService = TestBed.get(FilmListResolverService);
    expect(service).toBeTruthy();
  });
});
