import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmThumbnailComponent } from './film-thumbnail.component';

describe('FilmThumbnailComponent', () => {
  let component: FilmThumbnailComponent;
  let fixture: ComponentFixture<FilmThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
