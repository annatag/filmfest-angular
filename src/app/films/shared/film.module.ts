export interface IFilm {
  id: number;
  name: string;
  date: Date;
  time: string;
  director: string;
  country: string;
  duration: number;
  imageUrl: string;
  imdbUrl?: string;
  trailerUrl?: string;
  description?: string;

}
