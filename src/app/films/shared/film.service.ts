import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IFilm } from '.';

@Injectable()
export class FilmService {
 getFilms(): Observable<IFilm[]>{
   let subject = new Subject<IFilm[]>();
   setTimeout(() => {subject.next(FILMS); subject.complete(); }, 100);
   return subject;
  }

  /// Subject is a type of Observable

  getFilm(id: number): IFilm {
return FILMS.find(film => film.id === id);
  }
}


const FILMS: IFilm[] = [
  {
    id: 1,
    name: 'Once Upon a Time in Hollywood',
    date: new Date('7/26/2020'),
    time: '6:00 pm',
    director: 'Quentin Tarantino',
    country: 'USA',
    duration: 160,
    imageUrl: '/assets/images/Once Upon a Time_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt7131622/?ref_=nv_sr_srsg_0',
    trailerUrl: 'https://www.youtube.com/watch?v=Scf8nIJCvs4',
   // tslint:disable-next-line:max-line-length
    description: `Actor Rick Dalton gained fame and fortune by starring in a 1950s television Western, but is now struggling to find meaningful work in a Hollywood that he doesn't recognize anymore. He spends most of his time drinking and palling around with Cliff Booth, his easygoing best friend and longtime stunt double. Rick also happens to live next door to Roman Polanski and Sharon Tate -- the filmmaker and budding actress whose futures will forever be altered by members of the Manson Family.`
  },

  {
    id: 2,
    name: 'Farewell',
    date: new Date('07/20/2020'),
    time: '4:00 pm',
    director: 'Lulu Wang',
    country: 'Japan',
    duration: 128,
    imageUrl: '/assets/images/Farewell_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt0806029/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=kgJT_9IGqsA'
  },

  {
    id: 3,
    name: 'The Irishman',
    date: new Date('07/21/2020'),
    time: '5:00 pm',
    director: 'Martin Scorsese',
    country: 'USA',
    duration: 210,
    imageUrl: '/assets/images/Irishman_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt1302006/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=WHXxVmeGQUc'

  },

  {
    id: 4,
    name: 'The Souvenir',
    date: new Date('07/21/2020'),
    time: '8:00 pm',
    director: 'Joanna Hogg',
    country: 'USA',
    duration: 120,
    imageUrl: '/assets/images/The Souvenir_details.jpg',
    imdbUrl: 'https://www.imdb.com/find?q=The+Souvenir&ref_=nv_sr_sm',
    trailerUrl: 'https://www.youtube.com/watch?v=WTw1WGy0WyA'
  },

  {
    id: 5,
    name: 'The Two Popes',
    date: new Date('07/22/2020'),
    time: '8:00 pm',
    director: 'Fernando Meirelles',
    country: 'USA',
    duration: 126,
    imageUrl: '/assets/images/The Two Popes_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt8404614/?ref_=nv_sr_srsg_0',
    trailerUrl: 'https://www.youtube.com/watch?v=T5OhkFY1PQE'
  },

  {
    id: 6,
    name: 'Joker',
    date: new Date('07/23/2020'),
    time: '8:00 pm',
    director: 'Todd Phillips',
    country: 'USA',
    duration: 122,
    imageUrl: '/assets/images/Joker_details.jpeg',
    imdbUrl: 'https://www.imdb.com/title/tt7286456/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=zAGVQLHvwOY'
  },

  {
    id: 7,
    name: '1917',
    date: new Date('07/24/2020'),
    time: '7:30 pm',
    director: 'Sam Mendes',
    country: 'USA',
    duration: 119,
    imageUrl: '/assets/images/1917_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt8579674/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=YqNYrYUiMfg'
  },

  {
    id: 8,
    name: 'Little Women',
    date: new Date('07/25/2020'),
    time: '8:30 pm',
    director: 'Greta Gerwig',
    country: 'USA',
    duration: 135,
    imageUrl: '/assets/images/Little Women_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt3281548/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=AST2-4db4ic'
  },

  {
    id: 9,
    name: 'Ford Vs Ferrari',
    date: new Date('07/25/2020'),
    time: '6:00 pm',
    director: 'James Mangold',
    country: 'USA',
    duration: 152,
    imageUrl: '/assets/images/Ford_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt1950186/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=I3h9Z89U9ZA'
  },

  {
    id: 10,
    name: 'Marriage Story',
    date: new Date('07/27/2020'),
    time: '6:00 pm',
    director: 'Noah Baumbach',
    country: 'USA',
    duration: 137,
    imageUrl: '/assets/images/Marriage_details.jpg',
    imdbUrl: 'https://www.imdb.com/title/tt7653254/?ref_=fn_al_tt_1',
    trailerUrl: 'https://www.youtube.com/watch?v=BHi-a1n8t7M'
  }
];

