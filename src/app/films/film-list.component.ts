import { Component, OnInit } from '@angular/core';
import { FilmService } from './shared/film.service';
import { ToastrService} from 'src/app/common/toastr.service';
import { ActivatedRoute } from '@angular/router';
import {IFilm} from './shared/index';


@Component({
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})

export class FilmListComponent implements OnInit {
films: IFilm[];
constructor(private filmService: FilmService, private toastr: ToastrService, private route: ActivatedRoute) {
}

ngOnInit() {
  // this.filmService.getFilms().subscribe(films => {this.films = films});/WITHOUT RESOLVER
this.films = this.route.snapshot.data.films;
}
handleThumbnailClick(filmName){
this.toastr.success(filmName);
}

}


