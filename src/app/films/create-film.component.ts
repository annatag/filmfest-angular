import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
@Component({
  selector: 'app-create-film',
  templateUrl: './create-film.component.html',
  styleUrls: ['./create-film.component.css']
})
export class CreateFilmComponent implements OnInit {
  isDirty: boolean = true;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  cancel(){
this.router.navigate(['/films']);
  }

}
