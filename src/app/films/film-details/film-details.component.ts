import { Component, OnInit } from '@angular/core';
import { FilmService } from '../shared/film.service';
import {ActivatedRoute} from '@angular/router';
import { IFilm } from '../shared/index';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})
export class FilmDetailsComponent implements OnInit {
 film: IFilm;
  constructor(private filmService: FilmService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.film = this.filmService.getFilm(+this.route.snapshot.params['id']);
  }

}
