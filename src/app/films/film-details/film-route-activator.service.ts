
import {Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { FilmService } from '../shared/film.service';

@Injectable()
export class FilmRouteActivator implements CanActivate {
  constructor(private filmService: FilmService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot) {
   const filmExists = !!this.filmService.getFilm(+route.params.id);
   if (!filmExists) {
    this.router.navigate(['404']);
  }

   return filmExists;
}
}
