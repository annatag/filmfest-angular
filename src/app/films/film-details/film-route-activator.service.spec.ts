import { TestBed } from '@angular/core/testing';

import { FilmRouteActivatorService } from './film-route-activator.service';

describe('FilmRouteActivatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilmRouteActivatorService = TestBed.get(FilmRouteActivatorService);
    expect(service).toBeTruthy();
  });
});
