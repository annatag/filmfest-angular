import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';

export const userRoutes = [
  {path: 'profile', component: ProfileComponent},
  {path: 'login', component: LoginComponent}
  // /the route is /user/profile
]
