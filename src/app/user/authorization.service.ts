import { Injectable } from '@angular/core';
import { IUser } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  currentUser: IUser;
  constructor() {}

  loginUser(userName: string, password: string) {
    /// this is just hardcoded user for now, eventually will pass
    // the username and password to the server and check if it exists,
    // and will return the user's info and here it  will be se to current user
    // with this current user will always bbe logged in as Emil Ovanesov

    this.currentUser = {
      id: 1,
      userName: userName,
      firstName: 'Emil',
      lastName: 'Ovanesov'
    };
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  updateCurrentUser(firstName: string, lastName: string){
this.currentUser.firstName=firstName;
this.currentUser.lastName=lastName;

  }
}
