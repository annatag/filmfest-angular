import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { userRoutes } from './user.routes';
import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(userRoutes),
    ReactiveFormsModule
  ],

  declarations: [
    ProfileComponent,
    LoginComponent
  ],

  providers: [ ]

})
export class UserModule { }


// In the AppModule we import the BrowserModule, but in feature modules like this, we don't nee it.
// Another difference: for RouterModule will callbackify.forChild, in the AppModule, weCall .forRoot
