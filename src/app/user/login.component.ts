import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from './authorization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
userName;
password;
mouseoverLogin;
  constructor(private authService: AuthorizationService, private router: Router) { }

  ngOnInit() {
  }

  login(formValues) {
    this.authService.loginUser(formValues.userName, formValues.password);
    console.log(formValues);
    this.router.navigate(['films']);
  }

cancel(){
  this.router.navigate(['films']);
}

}
