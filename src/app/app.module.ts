
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';


import {FilmListComponent,
  FilmThumbnailComponent,
  FilmService,
  FilmDetailsComponent,
  CreateFilmComponent,
  FilmRouteActivator,
  FilmListResolverService
} from './films/index';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ToastrService } from './common/toastr.service';
import { appRoutes} from './routes';
import { Error404Component } from './error/error404.component';
import { AuthorizationService } from './user/authorization.service';


@NgModule({
  declarations: [
    AppComponent,
    FilmListComponent,
    FilmThumbnailComponent,
    NavComponent,
    FilmDetailsComponent,
    CreateFilmComponent,
    Error404Component,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    FilmService,
    ToastrService,
    FilmRouteActivator,
    FilmListResolverService,
    AuthorizationService,
    {provide: 'canDeactivateCreateFilm',
    useValue: checkDirtyState}],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function checkDirtyState(component: CreateFilmComponent) {
  if (component.isDirty) {
    return window.confirm('You have not saved the film info, do you really want to cancel?');
    }
  return true;
}
